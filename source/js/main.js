// от шаблона мусор
//= default/plugins.js
//= default/main.js

// от node.js мусор
//= ../node_modules/lazysizes/lazysizes.js
//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

// от библиотек мусор
//= library/jquery.ui.touch-punch.min.js

// мой мусор
$(document).ready(function () {
	var CartPlusMinus = $('.cart-plus-minus');
	$(".qtybutton").on("click", function() {
		var $button = $(this);
		var oldValue = $button.parent().find("input").val();
		if ($button.text() === "+") {
			var newVal = parseFloat(oldValue) + 1;
		} else {
			// Don't allow decrementing below zero
			if (oldValue > 1) {
				var newVal = parseFloat(oldValue) - 1;
			} else {
				newVal = 1;
			}
		}
		$button.parent().find("input").val(newVal);
	});


	$(".owl-recently").on('changed.owl.carousel initialized.owl.carousel', function (event) {
		$(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
			autoplay: true,
			loop: true,
			nav: false,
			autoplay: false,
			autoplayTimeout: 8000,
			items: 4,
			margin: 20,
			dots:false,
			responsiveClass:true,
			responsive:{
				0:{
					items: 2,
					center: true,
				},
				400:{
					items:2,
					center: true,
				},
				768:{
					items:3,
					center: true,
				},
				992:{
					items:4,
				},
				1200:{
					items:4,
				},
			}
		});


	$(window).scroll(function () {
		moveMinicart();
	});
	$(window).resize(function () {
		console.log("resize");
		moveMinicart();
	});
	moveMinicart ();

	function moveMinicart () {
		var scroll_wrapper = $('#cart-scroll');
		var scroll = scroll_wrapper.children();
		var desktop_wrapper = $('#cart-desktop');
		var desktop = desktop_wrapper.children();
		var mobile_wrapper = $('#cart-mobile');
		var mobile = mobile_wrapper.children();

		if ( window.innerWidth < 991 ) {
			// Вставить в мобилку
			if (mobile.lenght) return false;
				mobile_wrapper.append(desktop.detach());
		} else {
			if (desktop.lenght) return false;
			desktop_wrapper.append( mobile.detach() );
		}

		if ( $(window).scrollTop() > 100 ) {
			// Вставить в декстом с скролом
			if (scroll.lenght) {
				return false;
			}
			scroll_wrapper.append(desktop.detach());
		} else {
			// Вставить в декстом без скрола
			if (desktop.lenght) {
				return false;
			}
			desktop_wrapper.append( scroll.detach() );
		}
	}


	$('#modalRegistr').on('show.bs.modal', function (e) {
		$("#modalLog").modal('hide');
		$("body").addClass("modal-open");
	});
	$('#modalLog').on('show.bs.modal', function (e) {
		$("#modalRegistr").modal('hide');
		$("body").addClass("modal-open");
	});

	$('#modalRegistr').on('shown.bs.modal', function (e) {
		$("body").addClass("modal-open");
	});
	$('#modalLog').on('shown.bs.modal', function (e) {
		$("body").addClass("modal-open");
	});


});
